package start;
import presentation.View;
import javax.swing.*;

public class App {

	public static void main(String[] args) {
		JFrame frame = new View("Orders management");
		frame.setVisible(true);
	}
}
