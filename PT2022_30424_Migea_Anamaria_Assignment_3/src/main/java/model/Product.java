package model;

/**
 * @Author: Migea Anamaria
 * Product model class; mapped to the database
 * @Since: Apr 15, 2022
 */
public class Product {
    private int id;
    private String name;
    private String supplierName;
    private int stock;
    private double unitPrice;
    private double quantityPerUnit;

    public Product(){

    }
    public Product(int id, String name, String supplierName, int stock, double unitPrice, double quantityPerUnit) {
        this.id = id;
        this.name = name;
        this.supplierName = supplierName;
        this.stock = stock;
        this.unitPrice = unitPrice;
        this.quantityPerUnit = quantityPerUnit;
    }

    public Product(String name, String supplierName, int stock, double unitPrice, double quantityPerUnit) {
        this.name = name;
        this.supplierName = supplierName;
        this.stock = stock;
        this.unitPrice = unitPrice;
        this.quantityPerUnit = quantityPerUnit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getQuantityPerUnit() {
        return quantityPerUnit;
    }

    public void setQuantityPerUnit(double quantityPerUnit) {
        this.quantityPerUnit = quantityPerUnit;
    }
}
