package dao;

import model.OrderManagement;

/**
 * @Author: Migea Anamaria
 * Extends AbstractDAO
 * @Since: Apr 15, 2022
 */
public class OrderDAO extends AbstractDAO<OrderManagement> {
}
