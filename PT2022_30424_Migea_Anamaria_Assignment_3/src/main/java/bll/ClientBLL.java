package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.ClientAgeValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

/**
 * @Author: Migea Anamaria
 * This class encapsulates the client business logic
 * @Since: Apr 15, 2022
 */

public class ClientBLL {

	private List<Validator<Client>> validators;
	private ClientDAO clientDAO;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new ClientAgeValidator());

		clientDAO =  new ClientDAO();
	}

	/**
	 * <p>findClientByID method. . .
	 * </p>
	 * @param id the client's id
	 * @return the client that has that particular id
	 */
	public Client findClientById(int id) throws NoSuchElementException{
		Client st = clientDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The client with id = " + id + " was not found!");
		}
		return st;
	}

	/**
	 * <p>findAllClients method. . .
	 * </p>
	 * @return all the clients in the database client table
	 */
	public List<Client> findAllClients(){
		return clientDAO.findAll();
	}

	/**
	 * <p>insertClient method. . .
	 * </p>
	 * @param  client - this client need to be inserted
	 */
	public void insertClient(Client client) throws IllegalArgumentException{
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		clientDAO.insert(client);
	}

	/**
	 * <p>deleteClient method. . .
	 * </p>
	 * @param  id - the client having this particular id must be deleted
	 */
	public void deleteClient(int id){
		clientDAO.delete(id);
	}

	/**
	 * <p>updateClient method. . .
	 * </p>
	 * @param  client - the client having this particular id must be updated
	 */
	public void updateClient(Client client) throws IllegalArgumentException{
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		clientDAO.update(client);
	}
}
