package presentation;

import bll.ProductBLL;
import model.Product;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class ViewProduct extends JFrame {

    private JPanel contentPane;
    private JPanel tablePane;
    private View previousView;

    protected ControllerProduct controllerProduct = new ControllerProduct(this);

    private JComboBox operationComboBox;
    private  JButton doButton;

    private JTable table;

    private JLabel idLabel;
    private JTextField idTextField;
    private JLabel nameLabel;
    private JTextField nameTextField;
    private JLabel supplierNameLabel;
    private JTextField supplierNameTextField;
    private JLabel stockLabel;
    private JTextField stockTextField;
    private JLabel unitPriceLabel;
    private JTextField unitPriceTextField;
    private JLabel quantityPerUnitLabel;
    private JTextField quantityPerUnitTextField;

    private JButton button;

    private JLabel deleteInfoLabel;
    private JLabel updateInfoLabel;

    public ViewProduct(String name, View previousView) {
        super(name);
        this.previousView = previousView;
        this.prepareGui();
        this.setResizable(false);

        this.addWindowListener(new WindowAdapter() {
            //for closing
            @Override
            public void windowClosing(WindowEvent e) {
                previousView.revealMainFrame();
            }
        });
    }

    private void prepareGui(){
        this.contentPane = new JPanel();
        this.tablePane = new JPanel();
        this.setSize(800, 800);

        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.contentPane.setLayout(null);

        this.tablePane.setLayout(null);
        this.tablePane.setBounds(10,50,750,400);

        String[] operations = new String[]{"Add new product", "Edit product", "Delete product", "View all products"};
        this.operationComboBox = new JComboBox(operations);
        this.operationComboBox.setBounds(50, 10, 150,30);
        this.contentPane.add(operationComboBox);

        this.doButton = new JButton("Execute");
        this.doButton.setBounds(200, 10, 100,30);
        this.doButton.setActionCommand("DO_OP");
        this.doButton.addActionListener(this.controllerProduct);
        this.contentPane.add(this.doButton);

        initializeTable();

        this.deleteInfoLabel =  new JLabel("to delete, just select the row");
        this.deleteInfoLabel.setBounds(10, 500, 300,60);
        this.deleteInfoLabel.setVisible(false);
        this.deleteInfoLabel.setForeground(Color.red);
        this.contentPane.add(this.deleteInfoLabel);

        this.updateInfoLabel =  new JLabel("to edit, select the row, modify the desired text fields and then press 'Update'");
        this.updateInfoLabel.setBounds(350, 20, 500,20);
        this.updateInfoLabel.setVisible(false);
        this.updateInfoLabel.setForeground(Color.red);
        this.contentPane.add(this.updateInfoLabel);

        this.idLabel = new JLabel("id =");
        this.idTextField = new JTextField();
        this.nameLabel = new JLabel("name =");
        this.nameTextField = new JTextField();
        this.supplierNameLabel = new JLabel("supplier =");
        this.supplierNameTextField = new JTextField();
        this.stockLabel = new JLabel("stock =");
        this.stockTextField = new JTextField();
        this.unitPriceLabel = new JLabel("unit price =");
        this.unitPriceTextField = new JTextField();
        this.quantityPerUnitLabel = new JLabel("quantity per unit =");
        this.quantityPerUnitTextField = new JTextField();
        this.button = new JButton();

        this.setContentPane(this.contentPane);
    }

    public void initializeInvisibleLabelsTextFields(){
        this.idLabel.setVisible(false);
        this.idTextField.setVisible(false);
        this.nameLabel.setVisible(false);
        this.nameTextField.setVisible(false);
        this.supplierNameTextField.setVisible(false);
        this.supplierNameLabel.setVisible(false);
        this.stockLabel.setVisible(false);
        this.stockTextField.setVisible(false);
        this.unitPriceTextField.setVisible(false);
        this.unitPriceLabel.setVisible(false);
        this.quantityPerUnitTextField.setVisible(false);
        this.quantityPerUnitLabel.setVisible(false);
        this.button.setVisible(false);
        this.updateInfoLabel.setVisible(false);
    }

    public void prepareInsert(){
        this.tablePane.removeAll();

        this.tablePane.setLayout(null);

        this.idLabel.setBounds(100, 100, 50,30);
        this.idLabel.setVisible(true);
        this.tablePane.add(this.idLabel);
        this.idTextField.setBounds(125, 100, 100,30);
        this.idTextField.setVisible(true);
        this.tablePane.add(this.idTextField);

        this.nameLabel.setBounds(100, 150, 50,30);
        this.nameLabel.setVisible(true);
        this.tablePane.add(this.nameLabel);
        this.nameTextField.setBounds(145, 150, 200,30);
        this.nameTextField.setVisible(true);
        this.tablePane.add(this.nameTextField);

        this.supplierNameLabel.setBounds(100, 200, 60,30);
        this.supplierNameLabel.setVisible(true);
        this.tablePane.add(this.supplierNameLabel);
        this.supplierNameTextField.setBounds(165, 200, 200,30);
        this.supplierNameTextField.setVisible(true);
        this.tablePane.add(this.supplierNameTextField);

        this.stockLabel.setBounds(100, 250, 60,30);
        this.stockLabel.setVisible(true);
        this.tablePane.add(this.stockLabel);
        this.stockTextField.setBounds(155, 250, 250,30);
        this.stockTextField.setVisible(true);
        this.tablePane.add(this.stockTextField);

        this.unitPriceLabel.setBounds(100, 300, 90,30);
        this.unitPriceLabel.setVisible(true);
        this.tablePane.add(this.unitPriceLabel);
        this.unitPriceTextField.setBounds(175, 300, 250,30);
        this.unitPriceTextField.setVisible(true);
        this.tablePane.add(this.unitPriceTextField);

        this.quantityPerUnitLabel.setBounds(100, 350, 120,30);
        this.quantityPerUnitLabel.setVisible(true);
        this.tablePane.add(this.quantityPerUnitLabel);
        this.quantityPerUnitTextField.setBounds(210, 350, 250,30);
        this.quantityPerUnitTextField.setVisible(true);
        this.tablePane.add(this.quantityPerUnitTextField);

        this.button.setText("Insert");
        this.button.setBounds(350, 500, 100,30);
        this.button.setActionCommand("INSERT_OP");
        this.button.addActionListener(this.controllerProduct);
        this.button.setVisible(true);
        this.contentPane.add(this.button);

        this.contentPane.add(this.tablePane);
        this.setContentPane(this.contentPane);
    }

    public void prepareEdit(){
        this.updateInfoLabel.setVisible(true);
        this.nameLabel.setBounds(10, 500, 50,30);
        this.nameTextField.setVisible(true);
        this.contentPane.add(this.nameLabel);
        this.nameTextField.setBounds(55, 500, 200,30);
        this.nameLabel.setVisible(true);
        this.contentPane.add(this.nameTextField);

        this.supplierNameLabel.setBounds(10, 550, 60,30);
        this.supplierNameLabel.setVisible(true);
        this.contentPane.add(this.supplierNameLabel);
        this.supplierNameTextField.setBounds(75, 550, 200,30);
        this.supplierNameTextField.setVisible(true);
        this.contentPane.add(this.supplierNameTextField);

        this.stockLabel.setBounds(10, 600, 60,30);
        this.stockLabel.setVisible(true);
        this.contentPane.add(this.stockLabel);
        this.stockTextField.setBounds(65, 600, 250,30);
        this.stockTextField.setVisible(true);
        this.contentPane.add(this.stockTextField);

        this.unitPriceLabel.setBounds(10, 650, 90,30);
        this.unitPriceLabel.setVisible(true);
        this.contentPane.add(this.unitPriceLabel);
        this.unitPriceTextField.setBounds(85, 650, 250,30);
        this.unitPriceTextField.setVisible(true);
        this.contentPane.add(this.unitPriceTextField);

        this.quantityPerUnitLabel.setBounds(10, 700, 120,30);
        this.quantityPerUnitLabel.setVisible(true);
        this.contentPane.add(this.quantityPerUnitLabel);
        this.quantityPerUnitTextField.setBounds(120, 700, 250,30);
        this.quantityPerUnitTextField.setVisible(true);
        this.contentPane.add(this.quantityPerUnitTextField);

        this.button.setText("Update");
        this.button.setBounds(650, 500, 100,30);
        this.button.setActionCommand("UPDATE_OP");
        this.button.addActionListener(this.controllerProduct);
        this.button.setVisible(true);
        this.contentPane.add(this.button);

        this.setContentPane(this.contentPane);
    }

    public void initializeTable(){
        ProductBLL productBll = new ProductBLL();
        List<Product> allClients = productBll.findAllProducts();
        ReflectionJTable reflexionJTable = new ReflectionJTable();
        table = new JTable();
        table = reflexionJTable.retrieveTable(allClients);

        prepareTable(table);
    }

    public void prepareTable(JTable tableArg){
        this.tablePane.removeAll();

        JTableHeader header = tableArg.getTableHeader();
        header.setBackground(Color.yellow);
        JScrollPane pane = new JScrollPane(tableArg);
        tableArg.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableArg.getColumnModel().getColumn(0).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(1).setPreferredWidth(200);
        tableArg.getColumnModel().getColumn(2).setPreferredWidth(250);
        tableArg.getColumnModel().getColumn(3).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(4).setPreferredWidth(80);
        tableArg.getColumnModel().getColumn(5).setPreferredWidth(90);

        this.tablePane.add(pane);
        this.table =tableArg;

        this.tablePane.setLayout(new GridLayout(1,1));
        this.contentPane.add(this.tablePane);
        this.setContentPane(this.contentPane);
    }

    public JComboBox getOperationComboBox() {
        return operationComboBox;
    }

    public JLabel getDeleteInfoLabel() {
        return deleteInfoLabel;
    }

    public JTable getTable() {
        return table;
    }

    public JTextField getIdTextField() {
        return idTextField;
    }

    public JTextField getNameTextField() {
        return nameTextField;
    }

    public JTextField getSupplierNameTextField() {
        return supplierNameTextField;
    }

    public JTextField getStockTextField() {
        return stockTextField;
    }

    public JTextField getUnitPriceTextField() {
        return unitPriceTextField;
    }

    public JTextField getQuantityPerUnitTextField() {
        return quantityPerUnitTextField;
    }
}
