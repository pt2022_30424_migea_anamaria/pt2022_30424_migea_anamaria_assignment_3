package bll.validators;

import bll.ProductBLL;
import model.OrderManagement;
import model.Product;

import java.util.regex.Pattern;

/**
 * @Author: Migea Anamaria
 * This class validates the order's amount/quantity - under stock or not
 * This implements validator
 * @Since: Apr 15, 2022
 */
public class OrderAmountValidator implements Validator<OrderManagement> {
    private static final String AMOUNT_PATTERN = "(?!(0{2,})|(0[1-9]))([0-9]+)";

    @Override
    public void validate(OrderManagement order) throws IllegalArgumentException{
        Pattern pattern = Pattern.compile(AMOUNT_PATTERN);
        if (!pattern.matcher(String.valueOf(order.getAmount())).matches()) {
            throw new IllegalArgumentException("Amount is not a valid amount!");
        }
        ProductBLL productBLL = new ProductBLL();
        Product product = productBLL.findProductById(order.getProductId());
        if(product.getStock()< order.getAmount()){
            throw new IllegalArgumentException("Under-stock!");
        }
    }
}
