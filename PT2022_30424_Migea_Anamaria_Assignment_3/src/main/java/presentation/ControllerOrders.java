package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import model.Client;
import model.OrderManagement;
import model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class ControllerOrders implements ActionListener {
    private ViewOrders view;

    public ControllerOrders(ViewOrders v){
        this.view = v;
    }
    private OrderBLL orderBll = new OrderBLL();
    private ClientBLL clientBll = new ClientBLL();
    private ProductBLL productBll = new ProductBLL();

    public void selectRowClient(){
        view.getTableClients().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                int selectedCellValue = (int) view.getTableClients().getValueAt(view.getTableClients().getSelectedRow() ,0);
                view.getIdClientTextField().setText(String.valueOf(selectedCellValue));
            }
        });
    }
    public void selectRowProduct(){
        view.getTableProducts().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                int selectedCellValue = (int) view.getTableProducts().getValueAt(view.getTableProducts().getSelectedRow() ,0);
                view.getIdProductTextField().setText(String.valueOf(selectedCellValue));
            }
        });
    }

    private void orderInPdf(Client client,Product product, OrderManagement orderManagement) throws FileNotFoundException, DocumentException {
        String s = "bill" + orderManagement.getId() + "_c" + client.getId() + "_p" + product.getId();
        String content = "Name client: " + client.getName() + "\nProduct name: " + product.getName() + "\nAmount/quantity: " + orderManagement.getAmount() + "\nFinal price: " + orderManagement.getFinalPrice();

        Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream("src/main/resources/" + s + ".pdf"));
		document.open();

		Font fontT = FontFactory.getFont(FontFactory.COURIER, 18, BaseColor.BLACK);
		Chunk chunkT = new Chunk("Order: " + orderManagement.getId(), fontT);
		document.add(chunkT);

        document.add(new Paragraph(content));

		document.close();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.equals("MAKE_ORDER")){
            try{
                int idClient = Integer.parseInt(view.getIdClientTextField().getText());
                int idProduct = Integer.parseInt(view.getIdProductTextField().getText());
                int amount = Integer.parseInt(view.getAmountTextField().getText());
                try {
                    Product product = productBll.findProductById(idProduct);
                    Client client = clientBll.findClientById(idClient);
                    double finalPrice = product.getUnitPrice() * amount;
                    OrderManagement order = new OrderManagement(idClient,idProduct,amount,finalPrice);
                    int idOrder = orderBll.insertOrder(order);
                    order.setId(idOrder);
                    int newStock = product.getStock() - amount;
                    product.setStock(newStock);
                    productBll.updateProduct(product);
                    try {
                        orderInPdf(client,product,order);
                    } catch (FileNotFoundException | DocumentException ex) {
                        JOptionPane.showMessageDialog(this.view, "ERROR - creating bill","ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                    JOptionPane.showMessageDialog(this.view, "Order finalized.");
                    view.initializeTables();
                }catch (IllegalArgumentException exception){
                    JOptionPane.showMessageDialog(this.view, "UNDER STOCK!","ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }catch (NumberFormatException exception){
                JOptionPane.showMessageDialog(this.view, "ILLEGAL IDS","ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
