package presentation;

import bll.ClientBLL;
import model.Client;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class ViewClient extends JFrame{

    private JPanel contentPane;
    private JPanel tablePane;
    private View previousView;

    protected ControllerClient controllerClient = new ControllerClient(this);

    private JComboBox operationComboBox;
    private  JButton doButton;

    private JTable table;

    private JLabel idLabel;
    private JTextField idTextField;
    private JLabel nameLabel;
    private JTextField nameTextField;
    private JLabel addressLabel;
    private JTextField addressTextField;
    private JLabel emailLabel;
    private JTextField emailTextField;
    private JLabel ageLabel;
    private JTextField ageTextField;

    private JButton button;

    private JLabel deleteInfoLabel;
    private JLabel updateInfoLabel;

    public ViewClient(String name, View previousView) {
        super(name);
        this.previousView = previousView;
        this.prepareGui();
        this.setResizable(false);

        this.addWindowListener(new WindowAdapter() {
            //for closing
            @Override
            public void windowClosing(WindowEvent e) {
                previousView.revealMainFrame();
            }
        });
    }


    private void prepareGui(){
        this.contentPane = new JPanel();
        this.tablePane = new JPanel();
        this.setSize(800, 800);

        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.contentPane.setLayout(null);

        this.tablePane.setLayout(null);
        this.tablePane.setBounds(10,50,750,400);

        String[] operations = new String[]{"Add new client", "Edit client", "Delete client", "View all clients"};
        this.operationComboBox = new JComboBox(operations);
        this.operationComboBox.setBounds(50, 10, 150,30);
        this.contentPane.add(operationComboBox);

        this.doButton = new JButton("Execute");
        this.doButton.setBounds(200, 10, 100,30);
        this.doButton.setActionCommand("DO_OP");
        this.doButton.addActionListener(this.controllerClient);
        this.contentPane.add(this.doButton);

        initializeTable();

        this.deleteInfoLabel =  new JLabel("to delete, just select the row");
        this.deleteInfoLabel.setBounds(10, 500, 300,60);
        this.deleteInfoLabel.setVisible(false);
        this.deleteInfoLabel.setForeground(Color.red);
        this.contentPane.add(this.deleteInfoLabel);

        this.updateInfoLabel =  new JLabel("to edit, select the row, modify the desired text fields and then press 'Update'");
        this.updateInfoLabel.setBounds(10, 710, 500,60);
        this.updateInfoLabel.setVisible(false);
        this.updateInfoLabel.setForeground(Color.red);
        this.contentPane.add(this.updateInfoLabel);

        this.idLabel = new JLabel("id =");
        this.idTextField = new JTextField();
        this.nameLabel = new JLabel("name =");
        this.nameTextField = new JTextField();
        this.addressLabel = new JLabel("address =");
        this.addressTextField = new JTextField();
        this.emailLabel = new JLabel("email =");
        this.emailTextField = new JTextField();
        this.ageLabel = new JLabel("age =");
        this.ageTextField = new JTextField();
        this.button = new JButton();

        this.setContentPane(this.contentPane);
    }

    public void initializeTable(){
        ClientBLL clientBll = new ClientBLL();
        List<Client> allClients = clientBll.findAllClients();
        ReflectionJTable reflexionJTable = new ReflectionJTable();
        table = new JTable();
        table = reflexionJTable.retrieveTable(allClients);

        prepareTable(table);
    }

    public void initializeInvisibleLabelsTextFields(){
        this.idLabel.setVisible(false);
        this.idTextField.setVisible(false);
        this.nameLabel.setVisible(false);
        this.nameTextField.setVisible(false);
        this.addressLabel.setVisible(false);
        this.addressTextField.setVisible(false);
        this.emailLabel.setVisible(false);
        this.emailTextField.setVisible(false);
        this.ageLabel.setVisible(false);
        this.ageTextField.setVisible(false);
        this.button.setVisible(false);
        this.updateInfoLabel.setVisible(false);
    }

    public void prepareInsert(){
        this.tablePane.removeAll();

        this.tablePane.setLayout(null);

        this.idLabel.setBounds(100, 100, 50,30);
        this.idLabel.setVisible(true);
        this.tablePane.add(this.idLabel);
        this.idTextField.setBounds(125, 100, 100,30);
        this.idTextField.setVisible(true);
        this.tablePane.add(this.idTextField);

        this.nameLabel.setBounds(100, 150, 50,30);
        this.nameLabel.setVisible(true);
        this.tablePane.add(this.nameLabel);
        this.nameTextField.setBounds(145, 150, 200,30);
        this.nameTextField.setVisible(true);
        this.tablePane.add(this.nameTextField);

        this.addressLabel.setBounds(100, 200, 60,30);
        this.addressLabel.setVisible(true);
        this.tablePane.add(this.addressLabel);
        this.addressTextField.setBounds(165, 200, 200,30);
        this.addressTextField.setVisible(true);
        this.tablePane.add(this.addressTextField);

        this.emailLabel.setBounds(100, 250, 60,30);
        this.emailLabel.setVisible(true);
        this.tablePane.add(this.emailLabel);
        this.emailTextField.setBounds(155, 250, 250,30);
        this.emailTextField.setVisible(true);
        this.tablePane.add(this.emailTextField);

        this.ageLabel.setBounds(100, 300, 60,30);
        this.ageLabel.setVisible(true);
        this.tablePane.add(this.ageLabel);
        this.ageTextField.setBounds(135, 300, 250,30);
        this.ageTextField.setVisible(true);
        this.tablePane.add(this.ageTextField);

        this.button.setText("Insert");
        this.button.setBounds(350, 350, 100,30);
        this.button.setActionCommand("INSERT_OP");
        this.button.addActionListener(this.controllerClient);
        this.button.setVisible(true);
        this.tablePane.add(this.button);

        this.contentPane.add(this.tablePane);
        this.setContentPane(this.contentPane);
    }

    public void prepareEdit(){
        this.updateInfoLabel.setVisible(true);
        this.nameLabel.setBounds(10, 500, 50,30);
        this.nameTextField.setVisible(true);
        this.contentPane.add(this.nameLabel);
        this.nameTextField.setBounds(55, 500, 200,30);
        this.nameLabel.setVisible(true);
        this.contentPane.add(this.nameTextField);

        this.addressLabel.setBounds(10, 550, 60,30);
        this.addressLabel.setVisible(true);
        this.contentPane.add(this.addressLabel);
        this.addressTextField.setBounds(75, 550, 200,30);
        this.addressTextField.setVisible(true);
        this.contentPane.add(this.addressTextField);

        this.emailLabel.setBounds(10, 600, 60,30);
        this.emailLabel.setVisible(true);
        this.contentPane.add(this.emailLabel);
        this.emailTextField.setBounds(65, 600, 250,30);
        this.emailTextField.setVisible(true);
        this.contentPane.add(this.emailTextField);

        this.ageLabel.setBounds(10, 650, 60,30);
        this.ageLabel.setVisible(true);
        this.contentPane.add(this.ageLabel);
        this.ageTextField.setBounds(45, 650, 250,30);
        this.ageTextField.setVisible(true);
        this.contentPane.add(this.ageTextField);

        this.button.setText("Update");
        this.button.setBounds(350, 700, 100,30);
        this.button.setActionCommand("UPDATE_OP");
        this.button.addActionListener(this.controllerClient);
        this.button.setVisible(true);
        this.contentPane.add(this.button);

        this.setContentPane(this.contentPane);
    }

    public void prepareTable(JTable tableArg){
        this.tablePane.removeAll();

        JTableHeader header = tableArg.getTableHeader();
        header.setBackground(Color.yellow);
        JScrollPane pane = new JScrollPane(tableArg);
        tableArg.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableArg.getColumnModel().getColumn(0).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(1).setPreferredWidth(150);
        tableArg.getColumnModel().getColumn(2).setPreferredWidth(250);
        tableArg.getColumnModel().getColumn(3).setPreferredWidth(250);
        tableArg.getColumnModel().getColumn(4).setPreferredWidth(50);

        this.tablePane.add(pane);
        this.table =tableArg;

        this.tablePane.setLayout(new GridLayout(1,1));
        this.contentPane.add(this.tablePane);
        this.setContentPane(this.contentPane);
    }

    public JComboBox getOperationComboBox() {
        return operationComboBox;
    }

    public JTextField getIdTextField() {
        return idTextField;
    }

    public JTextField getNameTextField() {
        return nameTextField;
    }

    public JTextField getAddressTextField() {
        return addressTextField;
    }

    public JTextField getEmailTextField() {
        return emailTextField;
    }

    public JTextField getAgeTextField() {
        return ageTextField;
    }

    public JTable getTable() {
        return table;
    }

    public JLabel getDeleteInfoLabel() {
        return deleteInfoLabel;
    }
}
