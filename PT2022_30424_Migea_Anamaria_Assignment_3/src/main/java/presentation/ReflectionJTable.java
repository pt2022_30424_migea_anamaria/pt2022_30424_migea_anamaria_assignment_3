package presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.lang.reflect.Field;
import java.util.List;

public class ReflectionJTable<T> {

    public JTable retrieveTable(List<T> listObjects) {
        String[] col = new String[listObjects.get(0).getClass().getDeclaredFields().length];
        Object[][] data = new Object[listObjects.size()][col.length];
        int indexData = 0;
        for(Object object: listObjects){
            int j = 0;
            for (Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true); // set modifier to public
                Object value;
                try {
                    col[j]=field.getName();
                    value = field.get(object);
                    data[indexData][j] = value;
                    j++;

                } catch (IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            indexData++;
        }
        TableModel model = new DefaultTableModel(data, col)
        {
            public boolean isCellEditable(int row, int column)
            {
                return false;//This causes all cells to be not editable
            }
        };
        JTable table = new JTable(model);
        return table;
    }
}
