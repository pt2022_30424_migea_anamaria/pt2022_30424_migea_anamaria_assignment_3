package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

import javax.swing.*;

/**
 * @Author: Migea Anamaria
 * @Since: Apr 15, 2022
 * @Source http://www.java-blog.com/mapping-javaobjects-database-reflection-generics
 */

public abstract class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }
    /**
     * <p>createSelectQuery method. . . builds the generic select query using the name of the class stored in ‘type’ field
     * </p>
     * @param  field name of the column of the table
     * @return created query
     */
    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" WHERE ").append(field).append(" =?");
        return sb.toString();
    }

    /**
     * <p>createSelectAllQuery method. . . builds a generic query that selects all the records in a table
     * </p>
     * @return created query
     */
    private String createSelectAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName().toLowerCase());
        return sb.toString();
    }

    /**
     * <p>createInsertQuery method. . . builds a generic query that inserts the t object in the corresponding t table
     * </p>
     * @param  t record that will be inserted in the table
     * @return created query
     */
    private String createInsertQuery(T t){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" (");
        StringBuilder sbV = new StringBuilder();
        sbV.append("VALUES (");
        int i=0;
        for (Field field : type.getDeclaredFields()) {
            field.setAccessible(true); // set modifier to public
            try {
                sb.append(field.getName());
                sbV.append("'").append(field.get(t)).append("'");
                if(i<type.getDeclaredFields().length-1){
                    sb.append(", ");
                    sbV.append(", ");
                }
                else{
                    sb.append(") ");
                    sbV.append(") ");
                }
                i++;
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sb.append(sbV);
        return sb.toString();
    }

    /**
     * <p>createDeleteQuery method. . . builds a generic query to delete records in a table using a field name
     * </p>
     * @param  field name of the column of the table
     * @return created query
     */
    private String createDeleteQuery(String field){
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" WHERE ").append(field).append(" =?");
        return sb.toString();
    }

    /**
     * <p>delete method. . . a record in the corresponding t table will be deleted.
     * </p>
     * @param  id object's id
     */
    public void delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createDeleteQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * <p>findAll. . . selects all the records in a table
     * </p>
     * @return all the records
     */
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * <p>findById method. . . selects the record with a particular id in a table
     * </p>
     * @param  id the id of the searched record
     * @return the object having that particular id
     */
    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * <p>createObjects method. . . obtains the list of model objects of type T
     * </p>
     * @param  resultSet the result of the executed query
     * @return the object having that particular id
     */
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        Constructor[] ctors = type.getDeclaredConstructors();
        Constructor ctor = null;
        for (int i = 0; i < ctors.length; i++) {
            ctor = ctors[i];
            if (ctor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                ctor.setAccessible(true);
                T instance = (T)ctor.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(fieldName);
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException | InvocationTargetException | SQLException | IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * <p>insert method. . . inserts a record in the corresponding t table
     * </p>
     * @param  t object that will be inserted
     * @return the inserted id
     */
    public int insert(T t) {
        Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(createInsertQuery(t), Statement.RETURN_GENERATED_KEYS);
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
            JOptionPane.showMessageDialog(null, type.getName() + "DAO:insert " + e.getMessage(),"ERROR", JOptionPane.ERROR_MESSAGE);
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
    }

    /**
     * <p>createUpdateQuery method. . .  builds a generic query to update a record having a given id in a table
     * </p>
     * @param  t record that will pe updated
     * @return created query
     */
    private String createUpdateQuery(T t){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" SET ");
        int i=0,id=0;
        for (Field field : type.getDeclaredFields()) {
            field.setAccessible(true); // set modifier to public
            try {
                sb.append(field.getName()).append("=");
                sb.append("'").append(field.get(t)).append("'");
                if(i<type.getDeclaredFields().length-1){
                    sb.append(", ");
                }
                if(field.getName().equals("id")){
                    id= (int) field.get(t);
                }
                i++;
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sb.append(" WHERE id = ").append(id);
        return sb.toString();
    }

    /**
     * <p>update method. . . a record in the corresponding t table will be deleted.
     * </p>
     * @param  t object's id
     */
    public void update(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery(t);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate(query);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
}
