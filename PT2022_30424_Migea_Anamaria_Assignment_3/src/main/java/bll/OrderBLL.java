package bll;

import bll.validators.*;
import dao.OrderDAO;
import model.OrderManagement;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Migea Anamaria
 * This class encapsulates the orders business logic
 * @Since: Apr 15, 2022
 */
public class OrderBLL {

    private List<Validator<OrderManagement>> validators;
    private OrderDAO orderDAO;

    public OrderBLL(){
        validators = new ArrayList<Validator<OrderManagement>>();
        validators.add(new OrderAmountValidator());
        orderDAO = new OrderDAO();
    }

    /**
     * <p>findAllOrders method. . .
     * </p>
     * @return all the orders in the database ordermanagement table
     */
    public List<OrderManagement> findAllOrders(){
        return orderDAO.findAll();
    }

    /**
     * <p>insertProduct method. . .
     * </p>
     * @param  orderManagement - this order need to be inserted
     * @return the id of the inserted order
     */
    public int insertOrder(OrderManagement orderManagement) throws IllegalArgumentException{
        for (Validator<OrderManagement> v : validators) {
            v.validate(orderManagement);
        }
        return orderDAO.insert(orderManagement);
    }
}
