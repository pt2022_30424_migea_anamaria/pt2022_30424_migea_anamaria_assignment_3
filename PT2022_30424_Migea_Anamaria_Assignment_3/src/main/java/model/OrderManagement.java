package model;

/**
 * @Author: Migea Anamaria
 * Order model class; mapped to the database
 * @Since: Apr 15, 2022
 */
public class OrderManagement {
    private int id;
    private int clientId;
    private int productId;
    private int amount;
    private double finalPrice;

    public OrderManagement(){

    }

    public OrderManagement(int clientId, int productId, int amount, double finalPrice) {
        this.clientId = clientId;
        this.productId = productId;
        this.amount = amount;
        this.finalPrice = finalPrice;
    }

    public int getId() {
        return id;
    }

    public int getClientId() {
        return clientId;
    }

    public int getProductId() {
        return productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }
}
