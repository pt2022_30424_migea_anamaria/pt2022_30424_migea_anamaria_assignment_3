package dao;

import model.Product;

/**
 * @Author: Migea Anamaria
 * Extends AbstractDAO
 * @Since: Apr 15, 2022
 */
public class ProductDAO extends AbstractDAO<Product>{
}
