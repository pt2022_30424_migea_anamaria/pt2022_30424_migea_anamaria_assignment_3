package bll.validators;

import model.Product;

import java.util.regex.Pattern;

/**
 * @Author: Migea Anamaria
 * This class validates the product's quantity per unit
 * This implements validator
 * @Since: Apr 15, 2022
 */
public class QuantityPerUnitValidator implements Validator<Product>{

    private static final String QUANTITY_PER_UNIT_PATTERN = "(?!(0{2,})|(0[1-9]))(([0-9]+\\.[0-9]+)|([0-9]+))";

    @Override
    public void validate(Product product) throws IllegalArgumentException{
        Pattern pattern = Pattern.compile(QUANTITY_PER_UNIT_PATTERN);
        if (!pattern.matcher(String.valueOf(product.getQuantityPerUnit())).matches()) {
            throw new IllegalArgumentException("Quantity per unit is not a valid quantity per unit!");
        }
    }
}
