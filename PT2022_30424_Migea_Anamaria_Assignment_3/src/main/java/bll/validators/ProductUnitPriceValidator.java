package bll.validators;

import model.Product;

import java.util.regex.Pattern;

/**
 * @Author: Migea Anamaria
 * This class validates the product's unit price
 * This implements validator
 * @Since: Apr 15, 2022
 */
public class ProductUnitPriceValidator implements Validator<Product>{

    private static final String UNIT_PRICE_PATTERN = "(?!(0{2,})|(0[1-9]))(([0-9]+\\.[0-9]+)|([0-9]+))";

    @Override
    public void validate(Product product) throws IllegalArgumentException{
        Pattern pattern = Pattern.compile(UNIT_PRICE_PATTERN);
        if (!pattern.matcher(String.valueOf(product.getUnitPrice())).matches()) {
            throw new IllegalArgumentException("Unit price is not a valid unit price!");
        }
    }
}
