package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {

    private View view;

    public Controller(View v){
        this.view = v;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if(command.equals("CLIENT_OP") ){
            view.closeMainFrame();

            ViewClient viewClient = new ViewClient("Operations on client",view);
            viewClient.setVisible(true);

        }else if(command.equals("PRODUCT_OP")){
            view.closeMainFrame();

            ViewProduct viewProduct = new ViewProduct("Operations on product",view);
            viewProduct.setVisible(true);


        }else if(command.equals("PRODUCT_ORDERS_OP")){
            view.closeMainFrame();

            ViewOrders viewOrders = new ViewOrders("Operations on product",view);
            viewOrders.setVisible(true);
        }
    }
}
