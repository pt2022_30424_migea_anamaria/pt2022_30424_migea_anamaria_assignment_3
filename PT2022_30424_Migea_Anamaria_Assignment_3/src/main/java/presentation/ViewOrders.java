package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.OrderManagement;
import model.Product;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class ViewOrders extends JFrame {
    private JPanel contentPane;
    private JPanel tablePaneClients,tablePaneProducts,tablePaneOrders;
    private View previousView;
    protected ControllerOrders controllerOrders = new ControllerOrders(this);

    private JLabel amountLabel;
    private JTextField amountTextField;

    private JLabel idClientLabel;
    private JTextField idClientTextField;

    private JLabel idProductLabel;
    private JTextField idProductTextField;
    private JTable tableClients,tableProducts,tableOrders;

    private JButton makeOrderButton;

    private JLabel infoLabel,clientTableLabel, productTableLabel,orderTableLabel;

    public ViewOrders(String name, View previousView) {
        super(name);
        this.previousView = previousView;
        this.prepareGui();
        this.setResizable(false);

        this.addWindowListener(new WindowAdapter() {
            //for closing
            @Override
            public void windowClosing(WindowEvent e) {
                previousView.revealMainFrame();
            }
        });
    }

    private void prepareGui(){
        this.contentPane = new JPanel();
        this.tablePaneClients = new JPanel();
        this.tablePaneProducts = new JPanel();
        this.tablePaneOrders = new JPanel();
        this.setSize(2000, 800);

        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.contentPane.setLayout(null);

        this.tablePaneClients.setLayout(null);
        this.tablePaneProducts.setLayout(null);
        this.tablePaneOrders.setLayout(null);

        this.tablePaneClients.setBounds(10,75,700,300);
        this.tablePaneProducts.setBounds(800,75,700,300);
        this.tablePaneOrders.setBounds(800,400,400,300);

        initializeTables();

        this.infoLabel =  new JLabel("INSTRUCTION - > select one row from each table, introduce the amount (quantity) for the product then press 'Make order'");
        this.infoLabel.setForeground(Color.red);
        this.infoLabel.setBounds(10, 10, 1900,30);
        this.contentPane.add(this.infoLabel);

        this.clientTableLabel =  new JLabel("Client table");
        this.clientTableLabel.setBounds(10, 50, 1900,30);
        this.contentPane.add(this.clientTableLabel);

        this.productTableLabel =  new JLabel("Product table");
        this.productTableLabel.setBounds(800, 50, 1900,30);
        this.contentPane.add(this.productTableLabel);

        this.idClientLabel =  new JLabel("client id =");
        this.idClientLabel.setBounds(10, 400, 120,30);
        this.contentPane.add(this.idClientLabel);

        this.idClientTextField = new JTextField();
        this.idClientTextField.setBounds(70, 400, 100,30);
        this.idClientTextField.setEnabled(false);
        this.contentPane.add(this.idClientTextField);

        this.idProductLabel =  new JLabel("product id =");
        this.idProductLabel.setBounds(10, 450, 200,30);
        this.contentPane.add(this.idProductLabel);

        this.idProductTextField = new JTextField();
        this.idProductTextField.setBounds(80, 450, 100,30);
        this.idProductTextField.setEnabled(false);
        this.contentPane.add(this.idProductTextField);

        this.amountLabel = new JLabel("amount/quantity =");
        this.amountLabel.setBounds(200,430,200,30);
        this.contentPane.add(this.amountLabel);

        this.amountTextField = new JTextField();
        this.amountTextField.setBounds(310,430,200,30);
        this.contentPane.add(this.amountTextField);

        this.makeOrderButton = new JButton("Make order");
        this.makeOrderButton.setBounds(10, 600, 100,30);
        this.makeOrderButton.setActionCommand("MAKE_ORDER");
        this.makeOrderButton.addActionListener(this.controllerOrders);
        this.contentPane.add(this.makeOrderButton);

        this.setContentPane(this.contentPane);
    }

    public void initializeTables(){
        ClientBLL clientBll = new ClientBLL();
        List<Client> allClients = clientBll.findAllClients();
        ReflectionJTable reflexionJTable = new ReflectionJTable();
        tableClients = new JTable();
        tableClients = reflexionJTable.retrieveTable(allClients);

        ProductBLL productBll = new ProductBLL();
        List<Product> allProducts = productBll.findAllProducts();
        tableProducts = new JTable();
        tableProducts = reflexionJTable.retrieveTable(allProducts);

        OrderBLL orderBll = new OrderBLL();
        List<OrderManagement> allOrders = orderBll.findAllOrders();
        tableOrders = new JTable();
        tableOrders = reflexionJTable.retrieveTable(allOrders);

        prepareClientsTable(tableClients);
        prepareProductsTable(tableProducts);
        prepareOrdersTable(tableOrders);

        controllerOrders.selectRowProduct();
        controllerOrders.selectRowClient();
    }

    private void prepareOrdersTable(JTable tableArg){
        this.tablePaneOrders.removeAll();

        JTableHeader header = tableArg.getTableHeader();
        header.setBackground(Color.yellow);
        JScrollPane pane = new JScrollPane(tableArg);
        tableArg.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableArg.getColumnModel().getColumn(0).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(1).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(2).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(3).setPreferredWidth(100);
        tableArg.getColumnModel().getColumn(4).setPreferredWidth(100);

        this.tablePaneOrders.add(pane);
        this.tableOrders =tableArg;

        this.tablePaneOrders.setLayout(new GridLayout(1,1));
        this.contentPane.add(this.tablePaneOrders);
        this.setContentPane(this.contentPane);
    }

    private void prepareClientsTable(JTable tableArg){
        this.tablePaneClients.removeAll();

        JTableHeader header = tableArg.getTableHeader();
        header.setBackground(Color.yellow);
        JScrollPane pane = new JScrollPane(tableArg);
        tableArg.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableArg.getColumnModel().getColumn(0).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(1).setPreferredWidth(150);
        tableArg.getColumnModel().getColumn(2).setPreferredWidth(250);
        tableArg.getColumnModel().getColumn(3).setPreferredWidth(250);
        tableArg.getColumnModel().getColumn(4).setPreferredWidth(50);

        this.tablePaneClients.add(pane);
        this.tableClients =tableArg;

        this.tablePaneClients.setLayout(new GridLayout(1,1));
        this.contentPane.add(this.tablePaneClients);
        this.setContentPane(this.contentPane);
    }

    private void prepareProductsTable(JTable tableArg){
        this.tablePaneProducts.removeAll();

        JTableHeader header = tableArg.getTableHeader();
        header.setBackground(Color.yellow);
        JScrollPane pane = new JScrollPane(tableArg);
        tableArg.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableArg.getColumnModel().getColumn(0).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(1).setPreferredWidth(200);
        tableArg.getColumnModel().getColumn(2).setPreferredWidth(250);
        tableArg.getColumnModel().getColumn(3).setPreferredWidth(70);
        tableArg.getColumnModel().getColumn(4).setPreferredWidth(80);
        tableArg.getColumnModel().getColumn(5).setPreferredWidth(90);

        this.tablePaneProducts.add(pane);
        this.tableProducts =tableArg;

        this.tablePaneProducts.setLayout(new GridLayout(1,1));
        this.contentPane.add(this.tablePaneProducts);
        this.setContentPane(this.contentPane);
    }

    public JTable getTableClients() {
        return tableClients;
    }

    public JTable getTableProducts() {
        return tableProducts;
    }

    public JTextField getIdClientTextField() {
        return idClientTextField;
    }

    public JTextField getIdProductTextField() {
        return idProductTextField;
    }

    public JTextField getAmountTextField() {
        return amountTextField;
    }
}
