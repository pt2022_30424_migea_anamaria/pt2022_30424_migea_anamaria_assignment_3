package bll.validators;

/**
 * @Author: Migea Anamaria
 * Generic interface containing the method that validates the data introduced
 * @Since: Apr 15, 2022
 */
public interface Validator<T> {

	public void validate(T t);
}
