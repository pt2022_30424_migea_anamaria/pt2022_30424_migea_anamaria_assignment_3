package dao;

import model.Client;

/**
 * @Author: Migea Anamaria
 * Extends AbstractDAO
 * @Since: Apr 15, 2022
 */
public class ClientDAO extends AbstractDAO<Client>{

}
