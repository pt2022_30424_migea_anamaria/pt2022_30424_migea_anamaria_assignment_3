package presentation;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame{
    private JPanel contentPane;

    private JLabel selectOnWhichToPerformOperationsLabel;

    private JButton clientOperationsButton;
    private JButton productOperationsButton;
    private JButton creatingProductOrdersButton;

    private ImageIcon ordersIcon;
    private JLabel myImageLabel;

    protected Controller controller = new Controller(this);

    public View(String name) {
        super(name);
        this.prepareGui();
        this.setResizable(false);
    }

    private void prepareGui(){
        myImageLabel = new JLabel();
        myImageLabel.setIcon(ordersIcon);
        myImageLabel.setSize(500,500);

        this.contentPane = new JPanel();
        ordersIcon = new ImageIcon(this.getClass().getResource("/orders.jpg"));
        Image img = ordersIcon.getImage();
        Image imgScale = img.getScaledInstance(myImageLabel.getWidth(),myImageLabel.getHeight(),Image.SCALE_SMOOTH);
        ImageIcon scaledIcon = new ImageIcon(imgScale);
        myImageLabel.setIcon(scaledIcon);

        this.setSize(500, 500);
        // here's the part where I center the frame on screen
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.contentPane.setLayout(null);

        this.selectOnWhichToPerformOperationsLabel = new JLabel("Choose which operation you want to perform", JLabel.CENTER);
        Font fontLabel = new Font("Times New Roman", Font.BOLD, 20);
        this.selectOnWhichToPerformOperationsLabel.setFont(fontLabel);
        this.selectOnWhichToPerformOperationsLabel.setBounds(10,10,500,70);
        myImageLabel.add(this.selectOnWhichToPerformOperationsLabel);

        Font fontButtons = new Font("Times New Roman", Font.BOLD, 15);
        this.selectOnWhichToPerformOperationsLabel.setFont(fontLabel);

        this.clientOperationsButton = new JButton("Client");
        this.clientOperationsButton.setBounds(100, 120, 100,30);
        this.clientOperationsButton.setFont(fontButtons);
        this.clientOperationsButton.setActionCommand("CLIENT_OP");
        this.clientOperationsButton.addActionListener(this.controller);
        this.contentPane.add(this.clientOperationsButton);

        this.productOperationsButton = new JButton("Product");
        this.productOperationsButton.setBounds(300, 120, 100,30);
        this.productOperationsButton.setFont(fontButtons);
        this.productOperationsButton.setActionCommand("PRODUCT_OP");
        this.productOperationsButton.addActionListener(this.controller);
        this.contentPane.add(this.productOperationsButton);

        this.creatingProductOrdersButton = new JButton("Make orders");
        this.creatingProductOrdersButton.setBounds(150, 200, 200,30);
        this.creatingProductOrdersButton.setFont(fontButtons);
        this.creatingProductOrdersButton.setActionCommand("PRODUCT_ORDERS_OP");
        this.creatingProductOrdersButton.addActionListener(this.controller);
        this.contentPane.add(this.creatingProductOrdersButton);

        this.contentPane.add(myImageLabel);
        this.setContentPane(this.contentPane);
    }

    public void closeMainFrame(){
        setVisible(false);
    }

    public void revealMainFrame(){
        setVisible(true);
    }
}
