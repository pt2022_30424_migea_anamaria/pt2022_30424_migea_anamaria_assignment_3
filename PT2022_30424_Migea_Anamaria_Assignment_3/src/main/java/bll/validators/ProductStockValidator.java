package bll.validators;

import model.Product;

import java.util.regex.Pattern;

/**
 * @Author: Migea Anamaria
 * This class validates the product's stock
 * This implements validator
 * @Since: Apr 15, 2022
 */
public class ProductStockValidator implements Validator<Product>{

    private static final String STOCK_PATTERN = "(?!(0{2,})|(0[1-9]))([0-9]+)";

    @Override
    public void validate(Product product) throws IllegalArgumentException{
        Pattern pattern = Pattern.compile(STOCK_PATTERN);
        if (!pattern.matcher(String.valueOf(product.getStock())).matches()) {
            throw new IllegalArgumentException("Stock is not a valid stock!");
        }
    }
}
