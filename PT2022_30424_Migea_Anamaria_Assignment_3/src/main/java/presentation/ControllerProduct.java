package presentation;

import bll.ProductBLL;
import model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ControllerProduct implements ActionListener {

    private ViewProduct view;
    private ProductBLL productBll =  new ProductBLL();

    public ControllerProduct(ViewProduct v){
        this.view = v;
    }
    public void selectRowToDeleteFromTable(){
        view.getTable().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                int selectedCellValue = (int) view.getTable().getValueAt(view.getTable().getSelectedRow() ,0);
                System.out.println(selectedCellValue);
                productBll.deleteProduct(selectedCellValue);
                view.initializeTable();
            }
        });
    }
    public void selectRowToUpdate(){
        view.getTable().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                int id = (int) view.getTable().getValueAt(view.getTable().getSelectedRow() ,0);
                String nameString = (String) view.getTable().getValueAt(view.getTable().getSelectedRow() ,1);
                String supplierNameString = (String) view.getTable().getValueAt(view.getTable().getSelectedRow() ,2);
                int stock = (int) view.getTable().getValueAt(view.getTable().getSelectedRow() ,3);
                double unitPrice = (double) view.getTable().getValueAt(view.getTable().getSelectedRow() ,4);
                double quantityPerUnit = (double) view.getTable().getValueAt(view.getTable().getSelectedRow() ,5);
                view.getIdTextField().setText(String.valueOf(id));
                view.getNameTextField().setText(nameString);
                view.getSupplierNameTextField().setText(supplierNameString);
                view.getStockTextField().setText(String.valueOf(stock));
                view.getUnitPriceTextField().setText(String.valueOf(unitPrice));
                view.getQuantityPerUnitTextField().setText(String.valueOf(quantityPerUnit));
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        String operation = String.valueOf(view.getOperationComboBox().getSelectedItem());
        if(command.equals("DO_OP") ){
            view.initializeInvisibleLabelsTextFields();
            switch (operation){
                case "Add new product": view.getDeleteInfoLabel().setVisible(false);
                    view.prepareInsert();
                    break;
                case "Edit product": view.getDeleteInfoLabel().setVisible(false);
                    view.initializeTable();
                    view.prepareEdit();
                    selectRowToUpdate();
                    break;
                case "Delete product": view.getDeleteInfoLabel().setVisible(true);
                    view.initializeTable();
                    selectRowToDeleteFromTable();
                    break;
                case "View all products": view.getDeleteInfoLabel().setVisible(false);
                    view.initializeTable();
                    break;
            }
        }
        if(command.equals("INSERT_OP") || command.equals("UPDATE_OP")) {
            String idString = String.valueOf(view.getIdTextField().getText());
            String nameString = String.valueOf(view.getNameTextField().getText());
            String supplierNameString = String.valueOf(view.getSupplierNameTextField().getText());
            String stockString = String.valueOf(view.getStockTextField().getText());
            String unitPriceString = String.valueOf(view.getUnitPriceTextField().getText());
            String quantityPerUnitString = String.valueOf(view.getQuantityPerUnitTextField().getText());
            int id = 0, stock=0;
            double unitPrice = 0.0, quantityPerUnit=0.0;
            try {
                if (!idString.equals("")) {
                    id = Integer.parseInt(idString);
                }
                stock = Integer.parseInt(stockString);
                unitPrice = Double.parseDouble(unitPriceString);
                quantityPerUnit = Double.parseDouble(quantityPerUnitString);
                try {
                    if (command.equals("INSERT_OP")) {
                        if (!idString.equals("")) {
                            productBll.insertProduct(new Product(id, nameString, supplierNameString, stock, unitPrice,quantityPerUnit));
                            JOptionPane.showMessageDialog(this.view, "Product inserted.");
                        } else {
                            productBll.insertProduct(new Product(nameString, supplierNameString, stock, unitPrice,quantityPerUnit));
                            JOptionPane.showMessageDialog(this.view, "Product inserted.");
                        }
                    } else {
                        productBll.updateProduct(new Product(id,nameString, supplierNameString, stock, unitPrice,quantityPerUnit));
                        view.initializeTable();
                    }

                } catch (IllegalArgumentException exception) {
                    JOptionPane.showMessageDialog(null, "ILLEGAL ARGUMENTS", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NumberFormatException exception) {
                JOptionPane.showMessageDialog(null, "ILLEGAL ARGUMENTS", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
