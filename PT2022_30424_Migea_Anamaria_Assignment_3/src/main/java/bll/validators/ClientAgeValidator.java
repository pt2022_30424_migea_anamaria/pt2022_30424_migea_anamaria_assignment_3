package bll.validators;

import model.Client;

import java.util.regex.Pattern;

/**
 * @Author: Migea Anamaria
 * This class validates the client's age
 * This implements validator
 * @Since: Apr 15, 2022
 */
public class ClientAgeValidator implements Validator<Client> {
	private static final int MIN_AGE = 7;
	private static final int MAX_AGE = 90;

	private static final String AGE_PATTERN = "(?!(0{2,})|(0[1-9]))([0-9]+)";

	public void validate(Client t) throws IllegalArgumentException{
		Pattern pattern = Pattern.compile(AGE_PATTERN);
		if (!pattern.matcher(String.valueOf(t.getAge())).matches()) {
			throw new IllegalArgumentException("Client age is not a valid age!");
		}
		if (t.getAge() < MIN_AGE || t.getAge() > MAX_AGE) {
			throw new IllegalArgumentException("Client age limit is not respected!");
		}
	}

}
