package bll;

import bll.validators.ProductStockValidator;
import bll.validators.ProductUnitPriceValidator;
import bll.validators.QuantityPerUnitValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @Author: Migea Anamaria
 * This class encapsulates the product business logic
 * @Since: Apr 15, 2022
 */
public class ProductBLL {
    private List<Validator<Product>> validators;
    private ProductDAO productDAO;

    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductStockValidator());
        validators.add(new ProductUnitPriceValidator());
        validators.add(new QuantityPerUnitValidator());
        productDAO =  new ProductDAO();
    }

    /**
     * <p>findProductByID method. . .
     * </p>
     * @param id the product's id
     * @return the product that has that particular id
     */
    public Product findProductById(int id) throws NoSuchElementException{
        Product product = productDAO.findById(id);
        if (product == null) {
            throw new NoSuchElementException("The product with id = " + id + " was not found!");
        }
        return product;
    }

    /**
     * <p>findAllProducts method. . .
     * </p>
     * @return all the products in the database product table
     */
    public List<Product> findAllProducts(){
        return productDAO.findAll();
    }

    /**
     * <p>insertProduct method. . .
     * </p>
     * @param  product - this product need to be inserted
     */
    public void insertProduct(Product product) throws IllegalArgumentException{
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        productDAO.insert(product);
    }

    /**
     * <p>deleteProduct method. . .
     * </p>
     * @param  id - the product having this particular id must be deleted
     */
    public void deleteProduct(int id){
        productDAO.delete(id);
    }

    /**
     * <p>updateProduct method. . .
     * </p>
     * @param  product - the client having this particular id must be updated
     */
    public void updateProduct(Product product) throws IllegalArgumentException{
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        productDAO.update(product);
    }

}
