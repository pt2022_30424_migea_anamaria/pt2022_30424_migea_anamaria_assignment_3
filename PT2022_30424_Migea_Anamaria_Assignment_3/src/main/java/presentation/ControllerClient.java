package presentation;

import bll.ClientBLL;
import model.Client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ControllerClient implements ActionListener {

    private ViewClient view;
    private ClientBLL clientBll = new ClientBLL();

    public ControllerClient(ViewClient v){
        this.view = v;
    }
    public void selectRowToDeleteFromTable(){
        view.getTable().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                int selectedCellValue = (int) view.getTable().getValueAt(view.getTable().getSelectedRow() ,0);
                System.out.println(selectedCellValue);
                clientBll.deleteClient(selectedCellValue);
                view.initializeTable();
            }
        });
    }
    public void selectRowToUpdate(){
        view.getTable().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                int id = (int) view.getTable().getValueAt(view.getTable().getSelectedRow() ,0);
                String nameString = (String) view.getTable().getValueAt(view.getTable().getSelectedRow() ,1);
                String addressString = (String) view.getTable().getValueAt(view.getTable().getSelectedRow() ,2);
                String emailString = (String) view.getTable().getValueAt(view.getTable().getSelectedRow() ,3);
                int age = (int) view.getTable().getValueAt(view.getTable().getSelectedRow() ,4);
                view.getIdTextField().setText(String.valueOf(id));
                view.getNameTextField().setText(nameString);
                view.getAddressTextField().setText(addressString);
                view.getEmailTextField().setText(emailString);
                view.getAgeTextField().setText(String.valueOf(age));
            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        String operation = String.valueOf(view.getOperationComboBox().getSelectedItem());
        if(command.equals("DO_OP") ){
            view.initializeInvisibleLabelsTextFields();
            switch (operation){
                case "Add new client": view.getDeleteInfoLabel().setVisible(false);
                    view.prepareInsert();
                    break;
                case "Edit client": view.getDeleteInfoLabel().setVisible(false);
                    view.initializeTable();
                    view.prepareEdit();
                    selectRowToUpdate();
                    break;
                case "Delete client": view.getDeleteInfoLabel().setVisible(true);
                    view.initializeTable();
                    selectRowToDeleteFromTable();
                    break;
                case "View all clients": view.getDeleteInfoLabel().setVisible(false);
                    view.initializeTable();
                    break;
            }
        }
        if(command.equals("INSERT_OP") || command.equals("UPDATE_OP")){
            String idString = String.valueOf(view.getIdTextField().getText());
            String nameString = String.valueOf(view.getNameTextField().getText());
            String addressString = String.valueOf(view.getAddressTextField().getText());
            String emailString = String.valueOf(view.getEmailTextField().getText());
            String ageString = String.valueOf(view.getAgeTextField().getText());
            int id=0,age;
            try {
                if(!idString.equals("")){
                    id = Integer.parseInt(idString);
                }
                age = Integer.parseInt(ageString);
                try {
                    if(command.equals("INSERT_OP")){
                        if(!idString.equals("")){
                            clientBll.insertClient(new Client(id,nameString,addressString,emailString,age));
                            JOptionPane.showMessageDialog(this.view, "Client inserted.");
                        }else{
                            clientBll.insertClient(new Client(nameString,addressString,emailString,age));
                            JOptionPane.showMessageDialog(this.view, "Client inserted.");
                        }
                    }else {
                        clientBll.updateClient(new Client(id,nameString,addressString,emailString,age));
                        view.initializeTable();
                    }

                }catch (IllegalArgumentException exception){
                    JOptionPane.showMessageDialog(null, "ILLEGAL ARGUMENTS","ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }catch (NumberFormatException exception){
                JOptionPane.showMessageDialog(null, "ILLEGAL ARGUMENTS","ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
